# OSC-OmxPlayer 

for Raspi I - III

using minimal Raspian Jessie install

no X11 : display on the framebuffer

control 1 servo motor with PWM control pin on GPIO Pin 18 (phys pin 12), that can be fed by the RaspiIII if small enough (- on phys pin 6, +5v on phys pin 2) 

OSC-OmxPlayer script and documentation is released here by Olivier Meunier / Olm-e under the GNU-GPL V3.0 Licence, available here
http://www.gnu.org/licenses/gpl.txt
contact : 01[at]olm-e.be


### Install 

  * copy raspian .img to sd card with dd

  * configure :

~$ sudo passwd pi

~$ sudo raspi-config 

(expand system, choose locale and keyboard, etc... )

  * update system :

~$ sudo apt-get update

~$ sudo apt-get upgrade

  * install base libraries :

~$ sudo apt-get install python3-setuptools python3-dev python3-dbus omxplayer

  * install GPIO lib (using V2 beta ... it works ;) )

~$ wget https://github.com/metachris/RPIO/archive/v2.zip

~$ unzip *.zip

~$ cd RPIO-2/
 
~$ sudo python3 setup.py install

  * installing python-dbus-omxplayer wrapper, that greatly simplify the code

~$ cd ~

~$ wget https://github.com/willprice/python-omxplayer-wrapper/archive/develop.zip

~$ unzip develop.zip

~$ cd ~/python-omxplayer-wrapper-develop/

~$ sudo python3 setup.py install

the IP of the Raspi is writen at the end of the boot session,
or you can get it with
 
~$ sudo ifconfig eth0

if you want to set it fixed (and not with dhcp) 
you have to edit the /etc/dhcpcd.conf file and add at the end (with correct IPs)

<code>
  interface eth0
        static ip_address=192.168.1.135/24
        static routers=192.168.1.1
</code>

  * now enjoy the osc player :D



### Usage 

On the Raspberry pi, you transfer a video file encoded in H264 with the name "video.mp4" in the /home/pi/ directory

you can do it by network in ssh with the command : 
 - with X.X.X.X the local IP of the Raspi 
(this works on linux and OSX)

~$ cd /dir/of/video/

~$ scp video.mp4 pi@X.X.X.X:/home/pi/

we can now start the player

~$ cd ~

~$ sudo python3 osc-omxplayer.0.2.py &

if you want it autostart, add it to this file after the other commands (not after exit...) 

/etc/rc.local

### OSC commands :

  * /play 

unpause the player

  * /playerstart f

start playing at f second (float)

  * /playstop 0

pause

  * /playstop 1 

rewind and pause ~ 1-5 image

  * /shutter f

servo position between [0 - 1] (500 - 2000 ms pulse / 0 - 180° for most small servo )


  * /rebbot 1

reboot the RaspberryPi

  * /shutdown 1

shutdown the RaspberryPi for safe unplug

  * /file myvideofilename.mp4

set a new video file to play - not working at the moment





