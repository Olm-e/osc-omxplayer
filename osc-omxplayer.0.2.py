##################################################################
#                                                                #
#   OSC-OmxPlayer script and documentation is released here by   #
#   Olivier Meunier / Olm-e  contact : 01[at]olm-e.be            #
#   under the GNU-GPL V3.0 Licence, available here               #
#   http://www.gnu.org/licenses/gpl.txt                          #
#   find sources and documentation at                            #   
#    https://framagit.org/Olm-e/osc-omxplayer                    #
#                                                                #
##################################################################
###
#############
###
#######
###
#
###
#


from omxplayer import OMXPlayer
from time import sleep
from pyOSC import OSCServer
import sys
from RPIO import PWM
import subprocess




server = OSCServer( ("", 9999) )
server.timeout = 0
run = True
servo = PWM.Servo()
servo.set_servo(18, 500)
player = OMXPlayer('/home/pi/video.mp4', args=['--no-osd', '--loop'])
player.play()

def handle_timeout(self):
    self.timed_out = True


import types
server.handle_timeout = types.MethodType(handle_timeout, server)


#player seek to abs position with arg in ms and play 
def playerstart_callback(path, tags, args, source):
    user = ''.join(path.split("/"))
    print(user, args[0])
    player.pause()
    player.set_position(args[0])
    sleep(0.04) #some delay to let the player handle the next commands
    player.play()

#player pause with arg = 0 and rewind + pause with arg = 1
def playerstop_callback(path, tags, args, source):
    user = ''.join(path.split("/"))
    print(user, args[0])
    if args[0] == 1:
        player.pause
        player.set_position(0)
        sleep(0.04)
        player.play()
        sleep(0.2) #need some time to recover from the frame change, below 0.2s, dbus-omxplayer tends to not register next commands... 
        player.pause()

    else:
        if args[0] == 0:
            if player.is_playing():
                player.pause()

#shutter on servomotor on GPIO 18 ; close with arg = 1 open with arg = 0
def shutter_callback(path, tags, args, source):
    shutter = ''.join(path.split("/"))
    #print(shutter, args[0])
    angle = int((args[0]*75)+25)
    #print(angle)
    servo.set_servo(18, (angle*20))

    

#set the file to read from OSC, don't use whitespace or spec char in file names: from(azAZ09_-.) , can take 2sec to start...
# : rem : doesn't work atm ... 
 
def filer_callback(path, tags, args, source):
    filer = ''.join(path.split("/"))
    myfile = args[0]
    print(filer, myfile)
    #if player.is_playing():
    print("playing", player.get_filename())
    player.stop()
    player = OMXPlayer(myfile, args=['--no-osd', '--loop'])
    player.play()

#force playback
def play_callback(path, tags, args, source):
    #user = ''.join(path.split("/"))
    #print(user, args[0])
    player.play()

#shutdown and reboot the raspberrypi by OSC - to protect SDcard from wear
def reboot_callback(path, tags, args, source):
    rebootcommand = "/sbin/reboot -f"
    rebootprocess = subprocess.Popen(rebootcommand.split(), stdout=subprocess.PIPE)
    #rebootoutput = rebootprocess.communicate()[0]
    #print(rebootoutput)

def shutdown_callback(path, tags, args, source):
    player.stop()
    player.quit()
    server.close()
    command = "/sbin/shutdown -P now"
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    #output = process.communicate()[0]
    #print(output)

#register the callbacks for OSC adresses
server.addMsgHandler( "/playerstart", playerstart_callback )
server.addMsgHandler( "/playerstop", playerstop_callback )
server.addMsgHandler( "/shutter", shutter_callback )
server.addMsgHandler( "/file", filer_callback )
server.addMsgHandler( "/play", play_callback )
server.addMsgHandler( "/reboot", reboot_callback )
server.addMsgHandler( "/shutdown", shutdown_callback )

def each_frame():
    # clear timed_out flag
    server.timed_out = False
    # handle all pending requests then return
    while not server.timed_out:
        server.handle_request()


def quit():
    server.close()
    player.stop()
    player.quit()

#listen at 30Hz framerate 
while True :
    each_frame()
    sleep(0.03)

